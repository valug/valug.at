# valug.at hugo website

Clone this repository with submodules by running `git clone --recurse-submodules <URL>`, or `git submodule update --init` when you already cloned this repository.

Install hugo. Run `hugo serve` in this directory. Access the website on `localhost:1313`.

# how to edit/add content to the VALUG website

To edit existing content just open the markdown file using `$EDITOR` and make the changes you like.

The index of the site is in the `_index.md` file.

To create a new page, use `$EDITOR` to create the markdown file, or copy an existing one and make changes, or use `hugo new path/to/page.md`.


## structure

The static pages where all moved to the `pages` folder.

The event pages where all moved to the `events` folder. The `events` folder is special in the sense, that hugo is configured to create an ical feed
for all the pages in the `events` folder. The ical feed gets the event information from the frontmatter of the file:
```
---
title: "Testevent"
date: 2023-07-21T18:55:07+02:00
subtitle: ""
tags: []
location: VALUG Klubraum
start: 2023-07-14T18:30:00+02:00
end: 2023-07-15T00:00:00+02:00
---

More content ...
```
