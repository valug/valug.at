---
title: VALUG-Treffen - Hochvoltbatterie
start: 2024-07-12 18:30:00+01:00
end: 2024-07-12 23:30:00+01:00
location: VALUG-Klubraum, Alter Schl8hof Wels
---

Karl Zeilhofer stellt die Hochvoltbatterie der Zellhoff GmbH vor. Woraus besteht so eine Batterie und was sind die
Herausforderungen? Die Themengebiete werden Elektrotechnik, Elektronik, Firmware & Cloud sein.
