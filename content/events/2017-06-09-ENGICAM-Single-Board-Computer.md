---
date: '2017-06-11 14:10:58'
end: 2017-06-09 23:00:00+02:00
headcount: 9
location: VALUG-Klubraum, Alter Schl8hof Wels
slug: 2017-06-09-ENGICAM-Single-Board-Computer
start: 2017-06-09 18:30:00+02:00
title: 'VALUG Stammtisch - Live-Demo Industrie-Elektronik: CPU-Module, Touch-Display'

---
# ENGICAM - Single Board Computer

Walter Hofstädtler verschaffte uns einen Einblick in die Varianten und die Programmierung der ENGICAM Single Board Computer. 

Präsentationsfolien: http://www.zeilhofer.co.at/wiki/lib/exe/fetch.php?media=engicam_vortrag_20170609.pdf
