---
date: '2020-04-10 11:19:46'
end: 2020-04-10 23:00:00+02:00
location: Onlinekonferenz
slug: 2020-04-10-Das-Fediverse
start: 2020-04-10 18:30:00+02:00
title: VALUG Stammtisch - Das Fediverse

---
Online-Meeting mittels Jitsi unter https://meet.apronix.net/valug

## Vortragsfolien
* Slides: [Das Fediverse](VALUG-Vortrag%20Fediverse.html)
