---
title: VALUG-Treffen - Release Automatisierung
start: 2024-05-17 18:30:00+01:00
end: 2024-05-17 23:30:00+01:00
location: VALUG-Klubraum, Alter Schl8hof Wels
---

:warning: Achtung, dieser Termin wurde auf den 17.5. verschoben, am 10.5. findet kein Treffen statt.

Silwol ist für das Release Management bei OpenTalk zuständig und zeigt wie Release Management dort funktioniert. Im
Vortrag geht es um die etablierten Prozesse, die verwendete Software Tools und die Automatisierung bei der Erstellung
von Releases.
