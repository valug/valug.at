---
end: 2016-06-10 23:00:00+02:00
location: VALUG-Klubraum, Alter Schl8hof Wels
start: 2016-06-10 18:30:00+02:00
title: VALUG Spielestammtisch

---
Diesmal lassen wir es locker angehen und spielen einfach ein paar frei verfügbare Spiele. OpenTTD, Widelands oder was uns sonst einfällt.