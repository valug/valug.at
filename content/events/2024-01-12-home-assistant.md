---
title: VALUG-Treffen - Home Assistant
start: 2024-01-12 18:30:00+01:00
end: 2024-01-12 23:30:00+01:00
location: VALUG-Klubraum, Alter Schl8hof Wels
---

An diesem Abend geht es um Home Assistant, eine freie Software für das "Smart Home". Wir (notizblock, xeniter), zeigen
wie Home Assistant funktioniert und welche Integrationen möglich sind. Die groben Themen sind:

* Den eigenen Strom/Gas/Wasserverbrauch im Energie Dashboard visualisieren
* Sensoren einbinden und visualisieren
* Steuerung und Automatisierung
* Die Home Assistant Smartphone App
* Architektur von Home Assistant
* Xeniter's selbstgeschriebene Integration für seinen Staubsaugerroboter
