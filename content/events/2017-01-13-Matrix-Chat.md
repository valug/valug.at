---
date: '2017-01-13 16:58:54'
end: 2017-01-13 23:00:00+01:00
headcount: 14
location: VALUG-Klubraum, Alter Schl8hof Wels
slug: 2017-01-13-Matrix-Chat
start: 2017-01-13 18:30:00+01:00
title: VALUG Vortrag - Matrix Chat

---
* Slides: [presentation.html](presentation.html)
* Slides-Quelltext: https://gitlab.com/valug/matrix-chat-slides
