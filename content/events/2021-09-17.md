---
end: 2021-09-17 23:30:00+02:00
location: Alter Schl8hof Wels
start: 2021-09-17 18:30:00+02:00
title: VALUG-Stammtisch

---
Wir treffen uns im Alten Schl8hof Wels. Soweit möglich bleiben wir im Freien. Es gilt die 3G-Regel (geimpft, genesen oder getestet).