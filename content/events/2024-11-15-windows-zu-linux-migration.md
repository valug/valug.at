---
title: VALUG-Treffen - Diskussionsrunde zur Migration von Windows-Entwicklungsrechnern zu Linux
start: 2024-11-15 18:30:00+01:00
end: 2024-11-15 23:30:00+01:00
location: VALUG-Klubraum, Alter Schl8hof Wels
---

Wir diskutieren an Hand eines konkreten Beispiels über Herausforderungen,
die bei einer geplanten Migration von Windows-Entwicklungsrechnern auf ein
Linux-Betriebssystem stellen. Relevante Themen: Praxistipps zur Addministration,
möglichst stabiles Setup für leichteren Support, welche Admintools gibt es, um
Updates vorab zu testen und dann nach Freigabe auf mehr Geräte zu bringen usw.
