---
date: '2012-08-11 12:24:26'
end: 2012-08-10 23:30:00+02:00
headcount: 13
location: VALUG-Klubraum, Alter Schl8hof Wels
slug: '2012-08-10 Workshop: Participating in Free Software Projects'
start: 2012-08-10 18:30:00+02:00
title: 'Workshop: Participating in Free Software Projects'

---
Workshop mit laxity, notizblock und silwol.

Themen:
 * Community und Projektorganisation
 * Bug reports und Feature requests erstellen
 * Code beitragen
 * Diskussion

## Slides
 * **Git-Repository:** https://gitorious.org/valug/participating-in-free-software-projects-slides
 * **PDF:** [Download](slides-participating.pdf)
