---
title: VALUG-Treffen - Smartwatches mit Gadgetbridge
start: 2025-01-10 18:30:00+01:00
end: 2025-01-10 23:30:00+01:00
location: VALUG-Klubraum, Alter Schl8hof Wels
---

Gadgetbridge ist eine freie Android-App, die als Companion für eine lange Liste
an Smartwatches verwendet werden kann. Sie deckt die typischen Funktionen der
Companion-Apps der Hersteller ab, bietet an manchen Stellen mehr, an anderen
jedoch leider auch weniger Funktionalität. silwol zeigt, wie die App mit der
Smartwatch interagiert, welche Features und Einstellungen mit seiner Uhr
(teilweise noch nicht) möglich sind, und erzählt über die Erfahrungen damit
im Alltag.

https://gadgetbridge.org/
