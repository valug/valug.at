---
date: '2019-04-13 09:30:24'
end: 2019-04-12 23:00:00+02:00
headcount: 6
location: VALUG-Klubraum im Alten Schl8hof Wels
slug: 2019-04-12 Taskwarrior
start: 2019-04-12 18:30:00+02:00
title: VALUG-Stammtisch - Taskwarrior

---
* Vortrag von notizblock
* [Folien](https://nblock.org/static/talks/slides-valug-taskwarrior.pdf)
