---
date: '2016-09-09 12:15:38'
end: 2016-09-09 23:00:00+02:00
headcount: 10
location: VALUG-Klubraum, Alter Schl8hof Wels
slug: 2016-09-09-IPFS
start: 2016-09-09 18:30:00+02:00
title: IPFS - P2P-verteiltes Dateisystem

---
* Slides: [presentation.html](presentation.html)
* Slides-Quelltext: https://gitlab.com/valug/ipfs-slides
