---
title: VALUG-Treffen - WebAssembly und die Runtime "Wasmtime"
start: 2024-10-11 18:30:00+01:00
end: 2024-10-11 23:30:00+01:00
location: VALUG-Klubraum, Alter Schl8hof Wels
---

silwol hat sich aus beruflicher Motivation mit Hilfe der Runtime "Wasmtime"
ein wenig in WebAssembly eingearbeitet und zu Anschauungszwecken ein minimales
Plug-In-System gebaut, das er herzeigt und erklärt.
