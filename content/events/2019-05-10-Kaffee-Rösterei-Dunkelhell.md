---
date: '2019-05-11 22:27:41'
end: 2019-05-10 23:00:00+02:00
headcount: 17
location: "dunkelhell Kaffeer\xF6sterei, Anzengruberstra\xDFe 10/3, 4600 Wels\r"
slug: "2019-05-10-Kaffee-R\xF6sterei-Dunkelhell"
start: 2019-05-10 18:30:00+02:00
title: "VALUG Caf\xE9 - dunkelhell Kaffeer\xF6sterei"

---
Wir waren zu Besuch in der Kaffeerösterei Dunkelhell, die Peter betrieben wird. Peter war in der Anfangszeit aktiver VALUG-er, und hat uns in seine Rösterei eingeladen, um seinen DIY-Kafferöster zu begutachten und den Kaffee zu verkosten. Sehr lecker!
