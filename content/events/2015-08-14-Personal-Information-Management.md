---
date: '2015-08-15 21:34:33'
end: 2015-08-14 23:00:00+02:00
headcount: 11
location: VALUG-Klubraum, Alter Schl8hof Wels
slug: 2015-08-14-Personal-Information-Management
start: 2015-08-14 18:30:00+02:00
title: VALUG Stammtisch - Personal Information Management

---
## Linksammlung
 * https://github.com/strukturag/spreed-webrtc
 * https://owncloud.org/
 * http://radicale.org/
 * http://baikal-server.com/
 * https://davdroid.bitfire.at/
 * https://community.zarafa.com/
 * http://kolab.org/
