---
date: 2023-07-24 09:05:55+02:00
end: 2015-09-11 22:30:00+02:00
headcount: 10
location: VALUG-Klubraum / Kleiner Schl8hof-Saal, Alter Schl8hof Wels
start: 2015-09-11 18:00:00+02:00
subtitle: ''
tags: []
title: VALUG Stammtisch - Kalliope Aktionstag

---
Wir installieren gespendete Rechner für Asyl/Gästehäuser und Flüchtlingsunterkünfte.
Wenn wer für eine Unterkunft konkreten Bedarf an Geräten für Flüchtingshaus XY hat, ist der Freitag auch ein guter Zeitpunkt um sich vorzustellen und evtl. gleich ein paar Geräte zu bekommen.

Was wir brauchen?

- Monitore
- Laptop Netzteile (auch defekt wegen der Stecker usw..)
- Laptops
- Mäuse
- Geldspenden
- Verteilersteckdosen
