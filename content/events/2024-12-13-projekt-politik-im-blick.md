---
title: VALUG-Treffen - Vorstellung und Diskussion zum Projekt "Politik im Blick"
start: 2024-12-13 18:30:00+01:00
end: 2024-12-13 23:30:00+01:00
location: VALUG-Klubraum, Alter Schl8hof Wels
---

Niklas und Ralf stellen das Projekt [Politik im Blick](https://politik-im-blick.at/) vor, an dem sie
seit einiger Zeit entwickeln. Ziel des Projekts ist es, die regelmäßig
veröffentlichten Protokolle von Gemendieratssitzungen (teil-)automatisiert
zu analysieren und strukturieren, und diesen Datenbestand durchsuchbar und
analysierbar zu machen. Wir sprechen darüber, welche Möglichkeiten sich
daraus ergeben, und wie die Zukunft des Projekts aussehen könnte.
