---
date: 2023-07-24 09:05:55+02:00
end: 2015-03-13 23:00:00+01:00
headcount: 9
location: VALUG-Klubraum, Alter Schl8hof Wels
start: 2015-03-13 18:30:00+01:00
subtitle: ''
tags: []
title: "VALUG Stammtisch - Die Programmiersprache Rust - Infos, Einf\xFChrung mit\
  \ Beispielen und Werkzeug \"Cargo\""

---
Sprachfeatures werden anhand von Beispielen gezeigt. Außerdem wird auf das Werkzeug "Cargo" und das Online-Repository "crates.io" eingegangen. Informationen zur Entstehung und Besonderheiten außerhalb der Sprache selbst dürfen natürlich auch nicht fehlen.