---
date: '2021-06-13 11:18:51'
title: Chat
---

# Chat

Die VALUG benützt Chaträume, um online zu kommunizieren. Dort könnt ihr eure Fragen, Vorschläge etc. direkt an die Community richten. Der VALUG Channel ist ein freundlicher deutschsprachiger Tech-Channel, gegen ein paar Runden Off-Topic-Talk ist natürlich auch nichts einzuwenden.

## Matrix

Matrix-Channel: [**#valug:matrix.org**](https://matrix.to/#/#valug:matrix.org)

### Was ist Matrix Chat?

Matrix ist ein offenes Protokoll für Chat. Die Funktionalität ähnelt dem, was von Whatsapp oder Signal bekannt ist. Im Gegensatz zu diesen Applikationen ist Matrix jedoch, ähnlich wie E-Mail, dezentral organisiert, und daher nicht vom Gutwill einer einzelnen Organisation abhängig.

Matrix kann sowohl öffentlich einsehbar im Klartext, als auch verschlüsselt verwendet werden. Der [VALUG-Chatroom](https://matrix.to/#/#valug:matrix.org) ist öffentlich und unverschlüsselt, sodass alle mit möglichst niedriger Hürde direkt teilnehmen können. Der Raum ist auch für Gäste offen, sodass keine Registrierung bei einem Matrix-Provider notwendig ist.

## IRC

IRC-Channel: [#valug bei irc.libra.chat](irc://irc.libera.chat/#valug)


## Was ist IRC?

Internet Relay Chat ist ein textbasiertes Echtzeit-Kommunikationsmedium. Das heißt, was man reinschreibt, können alle anderen Teilnehmer des Kanals unmittelbar danach lesen. IRC als Technologie besteht bereits erheblich länger als andere Chat-Technologien wie Matrix, Whatsapp, Jabber oder ähnliches. Im Gegensatz zu diesen neueren Technologien ist daher z.B. keine automatische Speicherung des Chatverlaufs eingebaut.

### Wie nehme ich teil?

Verbreitete IRC-Programme sind:

 * [HexChat](https://hexchat.github.io/)
 * [Konversation](http://konversation.kde.org/)
 * [Pidgin](http://pidgin.im/) (auch für Windows verfügbar)
 * [Irssi](http://irssi.org/) (Konsolenprogramm)

## Hilfestellung für den IRC
Folgende Links allen IRC Newbies/Oldies den Umgang bzw. ersten Kontakt mit dem Chat etwas erleichtern:

 * [http://irchelp.org](http://irchelp.org)
 * [http://srcery.net/help](http://srcery.net/help)
 * [http://irc.cg.yu](http://irc.cg.yu)

Weitere Informationen bietet der [Wikipedia-Artikel **Internet Relay Chat**](https://de.wikipedia.org/wiki/Internet_Relay_Chat).

## Historisches

* Die Chaträume auf beiden Technologien **Matrix** und **IRC** waren in der Vergangenheit über eine so genannte **Bridge** verbunden. Diese wurde leider im August 2023 abgeschaltet, ob und wann sie wieder verfügbar sein wird ist unklar.
  * Infos bei matrix.org:
    * Infos zum Thema in [This Week in Matrix 2023-09-01](https://matrix.org/blog/2023/09/01/this-week-in-matrix-2023-09-01/#dept-of-status-of-matrix-face-with-th) (2023-09-01)
    * [Libera.Chat bridge temporarily unavailable](https://matrix.org/blog/2023/08/libera-bridge-disabled/) (2023-08-04)
    * [Postponing the Libera.Chat deportalling](https://matrix.org/blog/2023/07/postponing-libera-chat-deportalling/) (2023-07-28)
    * [Making Sure The Libera.Chat Bridge Keeps Working](https://matrix.org/blog/2023/07/make-sure-libera-bridge-keeps-working/) (2023-07-07)
    * [Deportalling from Libera Chat](https://matrix.org/blog/2023/07/deportalling-libera-chat/) (2023-07-04)

 
  * Infos bei libera.chat:
    * [Matrix Bridge Temporary Shutdown, a Retrospective](https://libera.chat/news/matrix-bridge-disabled-retrospective) (2023-08-10)
    * [Temporarily disabling the Matrix Bridge](https://libera.chat/news/temporarily-disabling-the-matrix-bridge) (2023-08-05)
    * [Delays in Disabling Matrix Portalling](https://libera.chat/news/deportalling-delay) (2023-07-08)
    * [Disabling Matrix Portalling](https://libera.chat/news/matrix-deportalling) (2023-07-03)
    * [Updates on the matrix<>IRC bridge](https://libera.chat/news/matrix-irc-bridge-updates) (2023-06-07)

* Leider musste der VALUG-Channel auf matrix.org 2021-06-13 auf einen neuen Channel umgezogen werden. Dieser ist nach wie vor unter der gleichen Adresse [#valug:matrix.org](https://matrix.to/#/#valug:matrix.org) erreichbar. Wer die Konversationen im alten Channel nachlesen möchte, findet sie im Channel [#valug-old:matrix.org](https://matrix.to/#/#valug-old:matrix.org). Die technischen Probleme hatten mit der IRC-Bridge und dem Umzug von Freenode zu Libera.chat zu tun. Die Bugs die uns getroffen haben sind https://github.com/matrix-org/matrix-appservice-irc/issues/1324 und https://github.com/matrix-org/matrix-appservice-irc/issues/1323 welche einen stale appservice-irc bot bewirkten, den wir nicht mehr losbekommen haben.
