---
date: '2019-05-07 12:36:55'
title: Kontakt
---
# Kontakt

Da die VALUG keine Organisation im herkömmlichen Sinne ist, gibt es auch nicht ''die'' Kontaktadresse. Über folgende Möglichkeiten kannst du Kontakt mit VALUG-Mitgliedern aufnehmen:
 * [Chat](/pages/Chat)
 * [Mailingliste](/pages/Mailingliste)
 * [Fediverse/Mastodon: @valug@floss.social](https://floss.social/@valug)
 * Besuche uns bei einer der [Veranstaltungen](/events). Üblicherweise treffen wir uns jeweils am 2. Freitag im Monat im [VALUG-Klubraum](/pages/Klubraum). Das Thema des Abends findest du auf der Seite [Veranstaltungen](/events).
