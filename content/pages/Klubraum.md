---
date: '2019-07-12 18:43:30'
title: Klubraum
---
# VALUG-Klubraum
Der Klubraum befindet sich im Gelände des Alten Schl8hofs in Wels, im hinteren Teil des Hofs.

## Karte
[OpenStreetMap-Karte](http://osm.org/go/0JK@e9_tv--)

## Adresse
Dragonerstraße 22, 4600 Wels

## Ausstattung
 * Internet
 * Strom
 * Video-Beamer
 * Video-Leinwand
 * Stereo-Anlage
 * Werkzeug (Lötkolben...) Bastelteile (jede Menge Ersatzteile usw)
 * Kühlschrank
 * [Getränke](/pages/Getränke)

## Bilder
![attachment:beamer.jpg](/files/beamer.jpg)

![attachment:DSC_3775.JPG](/files/DSC_3775.JPG)

![attachment:DSC_3773.JPG](/files/DSC_3769.JPG)

![attachment:DSC_3774.JPG](/files/DSC_3774.JPG)
