---
date: '2018-05-15 10:37:06'
title: "Themenvorschl\xE4ge"
---
# Themenvorschläge
 * Eine Einführung/Workshop zu XML und XPath
 * Digitale Selbstverteidigung (Erfahrungsaustausch und Diskussion, …) (Wunsch von notizblock)
 * Der Umgang mit Passwörtern und anderen Geheimnissen (Erfahrungsaustausch und Diskussion, …) (Wunsch von notizblock)
 * Personal Information Management (Erfahrungsaustausch, …) (Wunsch von notizblock)
 * Persönliche Zeitplanung (Erfahrungsaustausch, Diskussion, …) (Wunsch von lukanz, notizblock, …)
 * Samba4 Features und Einrichten (Wunsch von silwol)
 * Raspberry Pi und andere Kleincomputer (ouya, mediencenter...) (Wunsch von silwol)
 * Clonezilla Backups usw (Wunsch von nudels)
 * Xbmc und Konsorten (Wunsch von nudels)
 * Hardwaremods zb embedded Boards (Wunsch von nudels)
 * E-Mail mit mutt (Wunsch von laxity, notizblock)
 * Skripte vor allem um Sachen zu automatisieren zb Trim usw (nudels)
 * Pogoplug Pro und andere Boards (nudels)
 * Packaging für Linux Distributionen (Debian, Ubuntu, Arch, …) (Wunsch von notizblock, silwol, nudels)
 * OpenWRT und andere freie Software für Router (Wunsch von notizblock)
 * Debian anpassen zb netinstall an seine Bedürfnisse evtl. selber eine auf Debian basierende Distribution erstellen (nudels)
 * Unterschiede der verschiedenen Fenstermanager ala fluxbox, openbox, icewm, i3wm, awesome usw. (nudels)

# Bereits realisiert
 * Ansible, Puppet, Chef, … (2017-02-10)
 * Workshop: Tor (2012-10)
 * Workshop: Lizenzdschungel (2012-11)
 * SHA1 und OpenPGP/GnuPG (2013-01)
 * VPN unter GNU/Linux (2013-02, mariok)
 * C64 Einführung, Vorführung, Tiefgang in Komponenten(MOS 6502 CPU), Disk/Backup-party mit SD2IEC Modul (2013-04, mdipolt)
 * Finanzverwaltung mit freier Software (GnuCash, …) (2013-07)
 * Btrfs - was bringt es und wie setze ich die Fähigkeiten richtig ein? (2013-12)
 * Diskussionsrunde: digitales Erben (2014-01)
 * Full Disk Encryption -- Vorteile, was zu beachten ist, Einrichtung mit LUKS (und ggf. TrueCrypt) (2014-04, lfodh)
 * Android rooting & ROMing -- Vorteile, Gefahren, Hilfe zur Selbsthilfe (2014-07, lfodh)
 * Fitnesstracker - Aufzeichnung und Auswertung der sportlichen Aktivitäten (2014, notizblock)
